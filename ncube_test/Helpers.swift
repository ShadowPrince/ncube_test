//
//  Helpers.swift
//  ncube_test
//
//  Created by shdwprince on 3/2/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import Foundation
import UIKit

extension UIResponder {
    func performChainAction(_ sel: Selector, sender: Any) {
        var responder = self
        while let next = responder.next {
            if responder.responds(to: sel) {
                responder.perform(sel, with: sender)
                return
            }
            
            responder = next
        }
    }
}
