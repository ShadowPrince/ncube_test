//
//  ViewController.swift
//  ncube_test
//
//  Created by shdwprince on 3/2/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import UIKit

class ItemsViewController: UIViewController {
    var dataSource: ItemTableViewDataSource?
    @IBOutlet weak var tableView: UITableView!

    func setupTableView(with store: ItemStore, type: ItemTableType) {
        self.dataSource = ItemTableViewDataSource(store: store, type: type)
        self.tableView.dataSource = self.dataSource
        self.tableView.register(UINib.init(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: self.dataSource!.cellId)
    }
}

class StoreViewController: ItemsViewController {
    @IBOutlet weak var bucketTotalButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView(with: Store.shared, type: .store)
    }

    func buyItem(_ sender: Any) {
        let item = sender as! Item

        if Store.shared.removeItem(item) {
            Bucket.shared.appendItem(item.single())
            self.reload()
        }
    }

    @IBAction func unwindFromBucket(_ segue: UIStoryboardSegue) {
        self.reload()
    }

    func reload() {
        self.tableView.reloadData()
        self.bucketTotalButton.setTitle("Bucket (\(Bucket.shared.total) total)", for: .normal)
    }
}

class BucketViewController: ItemsViewController {
    override func viewDidLoad() {
        self.setupTableView(with: Bucket.shared, type: .bucket)
    }

    func removeItem(_ sender: Any) {
        let item = sender as! Item

        if Bucket.shared.removeItem(item) {
            Store.shared.appendItem(item.single())
            self.tableView.reloadData()
        }
    }
}
