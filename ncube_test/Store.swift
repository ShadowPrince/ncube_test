//
//  Stores.swift
//  ncube_test
//
//  Created by shdwprince on 3/2/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import Foundation

class ItemStore {
    // persistent store keep items with 0 amount, nonPersistent - not
    // first used for store, second for bucket
    enum StoreType {
        case persistent
        case nonPersistent
    }

    var items = [Item]()
    let type: StoreType

    required init(type: StoreType) {
        self.type = type

        if type == .persistent {
            for i in 0...20 {
                self.items.append(Item(id: i,
                                       name: "Item at number \(i+1)",
                                       price: Int(arc4random_uniform(100)) + 15,
                                       amount: UInt(arc4random_uniform(16)) + 4
                ))
            }
        }
    }

    func appendItem(_ item: Item) {
        if let existing = self.items.first(where: { $0 == item }) {
            existing.amount += 1
        } else {
            self.items.append(item)
        }
    }

    func removeItem(_ item: Item) -> Bool {
        // in the project this small we don't really care for the errors like these
        guard let existingIndex = self.items.index(of: item) else { return false }
        
        let existing = self.items[existingIndex]
        guard existing.amount > 0 else { return false }
        
        existing.amount -= 1
        if existing.amount == 0 && self.type == .nonPersistent {
            self.items.remove(at: existingIndex)
        }

        return true
    }

    var total: Int {
        return self.items.reduce(0, { $0 + ($1.price * Int($1.amount)) })
    }

    var count: Int {
        return self.items.count
    }

    subscript (idx: Int) -> Item? {
        return self.items[idx]
    }
}

class Store {
    static let shared = ItemStore(type: .persistent)
}

class Bucket {
    static let shared = ItemStore(type: .nonPersistent)
}
