//
//  ItemTableViewCell.swift
//  ncube_test
//
//  Created by shdwprince on 3/2/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import Foundation
import UIKit

enum ItemTableType {
    case store
    case bucket
}

class ItemTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!

    var item: Item?
    var type: ItemTableType = .store

    func setItem(_ item: Item, with type: ItemTableType) {
        self.type = type
        self.item = item

        self.nameLabel.text = "\(item.name) (\(item.price))"
        self.quantityLabel.text = "\(item.amount)"
        self.actionButton.setTitle(type == .store ? "Add" : "Remove", for: .normal)
    }

    @IBAction func action(_ sender: Any) {
        switch self.type {
        case .store:
            self.performChainAction(#selector(StoreViewController.buyItem(_:)), sender: self.item as Any)
        case .bucket:
            self.performChainAction(#selector(BucketViewController.removeItem(_:)), sender: self.item as Any)
        }
    }
}

class ItemTableViewDataSource: NSObject, UITableViewDataSource {
    let type: ItemTableType
    let cellId: String = "Cell"
    let store: ItemStore

    init(store: ItemStore, type: ItemTableType) {
        self.store = store
        self.type = type
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.store.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId) as! ItemTableViewCell
        cell.setItem(self.store[indexPath.row]!, with: self.type)

        return cell
    }
}
