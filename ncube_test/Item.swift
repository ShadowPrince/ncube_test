//
//  Item.swift
//  ncube_test
//
//  Created by shdwprince on 3/2/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import Foundation

class Item {
    let id: Int
    let name: String
    let price: Int
    var amount: UInt

    init(id: Int, name: String, price: Int, amount: UInt) {
        self.id = id
        self.name = name
        self.price = price
        self.amount = amount
    }

    // used for appending
    func single() -> Item {
        return Item(id: self.id, name: self.name, price: self.price, amount: 1)
    }
}

extension Item: Equatable {
    static func == (lhs: Item, rhs: Item) -> Bool {
        return lhs.id == rhs.id
    }
}

